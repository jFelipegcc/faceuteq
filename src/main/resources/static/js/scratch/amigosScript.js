$(document).ready(function () {

    let token = $("meta[name='_csrf']").attr("content");
    let header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });

    $.ajax({
        url: "http://localhost:8080/files/getSolisR",
        type: "POST",
        dataType: 'json',
        success: function (solis) {
            console.log(solis);
            if (solis.length > 0) {
                $("#sr").text("Solicitudes recibidas")
            }
            let cont2;
//            console.log("aaaaaa", solis);
            solis.forEach(function (soli) {
                console.log("aaa", soli.envia);


                cont2 = "";
                
                cont2 += '<div class="card bg-gray-dark">'
                cont2 += '<div class="card-body">'
                cont2 += '<div class="row">'
                cont2 += '<div class="col-9">'
                cont2 += '<div class="d-flex align-items-center align-self-start">'
                cont2 += '<h3 class="mb-0"><h6 class="preview-subject">' + soli.envia.nombre_us + ' ' + soli.envia.apellido_us + '</h6></h3>'
                cont2 += '</div>'
                if (soli.envia.carrera_us !== null) {
                    cont2 += '<h6 class="text-muted">' + soli.envia.carrera_us + '</h6>'
                }
                cont2 += '</div>'

                cont2 += '<div class="col-3">'

                let avatar
                if (soli.envia.avatar == null) {
                    avatar = "uteq.png"
                } else {
                    avatar = soli.envia.avatar
                }
                cont2 += '<div class="icon icon-box-success">'
                cont2 += '<img src="/images/avatars/' + avatar + '" alt="image"  style=" width: 90px;height: 90px;border-radius: 1000px; position: absolute;">'
                cont2 += '</div>'
                cont2 += '</div>'
                cont2 += '</div>'

                cont2 += '<div class="d-flex ">'



                cont2 += '<button type="submit" onclick="aceptarSolicitud(' + soli.envia.id_usuario + ')" class="btn btn-success mr-2">Aceptar Solicitud</button>'

                cont2 += '<a class="btn btn-dark" href="/private/verUsuario/' + soli.envia.id_usuario + '">Ver Perfil</a>'
                cont2 += '</div>'
                cont2 += '</div>'
                cont2 += '</div>'



                $("#divUsuarioSoli").append(cont2);

            })

        },
        error: function (err) {
            console.error(err);
        }
    });

    let miId;
    let miData;
    $.ajax({
        url: "http://localhost:8080/files/miInfo",
        type: "POST",
        dataType: 'json',
        success: function (yo) {
            console.log("a",yo);
            miId = yo.id_usuario;
            miData=yo.amigos;
        },
        error: function (err) {
            //console.error(err);
        }
    });


    $.ajax({
        url: "http://localhost:8080/files/getMinis",
        type: "POST",
        dataType: 'json',
        success: function (minis) {
            console.log(minis);
            $.ajax({
                url: "http://localhost:8080/files/getAmigos",
                type: "POST",
                dataType: 'json',
                success: function (a) {
                    let amigos = miData;
                    console.log(amigos);
                    $.ajax({
                        url: "http://localhost:8080/files/getSolis",
                        type: "POST",
                        dataType: 'json',
                        success: function (solis) {
                            console.log(solis);
                            let cont;

                            minis.forEach(function (mini) {
                                cont = "";
                                //                console.log("uy",miId);
                                //              console.log("uy2",mini.id_usuario);
                                if (mini.id_usuario !== miId && mini.nombre_us.length !== 0) {
                                    cont += '<div class="card bg-gray-dark">'
                                    cont += '<div class="card-body">'
                                    cont += '<div class="row">'
                                    cont += '<div class="col-9">'
                                    cont += '<div class="d-flex align-items-center align-self-start">'
                                    cont += '<h3 class="mb-0"><h6 class="preview-subject">' + mini.nombre_us + ' ' + mini.apellido_us + '</h6></h3>'

                                    cont += '</div>'
                                    if (mini.carrera_us !== null) {
                                        cont += '<h6 class="text-muted">' + mini.carrera_us + '</h6>'
                                    }
                                    cont += '</div>'

                                    cont += '<div class="col-3">'

                                    let avatar
                                    if (mini.avatar == null) {
                                        avatar = "uteq.png"
                                    } else {
                                        avatar = mini.avatar
                                    }
                                    cont += '<div class="icon icon-box-success">'
                                    cont += '<img src="/images/avatars/' + avatar + '" alt="image"  style=" width: 90px;height: 90px;border-radius: 1000px; position: absolute;">'
                                    cont += '</div>'
                                    cont += '</div>'
                                    cont += '</div>'

                                    cont += '<div class="d-flex ">'

                                    let isSoli = 0;
                                    let isFriend = 0;
                                    solis.forEach(function (soli) {
                                        if (soli.recibe_solicitud == mini.id_usuario) {
                                            isSoli++;
                                        }
                                    })
                                    amigos.forEach(function (amigo) {
                                        if (amigo.amigo == mini.id_usuario) {
                                            isFriend++;
                                        }
                                    })
                                    
                                        //console.log(isFriend,mini.id_usuario);
                                    if (isSoli > 0) {
                                        cont += '<button type="submit" onclick="eliminarSolicitud(' + mini.id_usuario + ')" class="btn btn-warning mr-2">Cancelar Solicitud</button>'
                                    } else if (isFriend > 0) {
                                        cont += '<button type="submit" onclick="eliminarAmigo(' + mini.id_usuario + ')" class="btn btn-danger mr-2">Eliminar amigo</button>'
                                    } else {
                                        cont += '<button type="submit" onclick="mandarSolicitud(' + mini.id_usuario + ')" class="btn btn-primary mr-2">Enviar Solicitud</button>'

                                    }
                                    cont += '<a class="btn btn-dark" href="/private/verUsuario/' + mini.id_usuario + '">Ver Perfil</a>'

                                    isFriend = 0;

                                    cont += '</div>'
                                    cont += '</div>'
                                    cont += '</div>'


                                    $("#divUsuario").append(cont);
                                }
                            })

                        },
                        error: function (err) {
                            console.error(err);
                        }
                    });

                },
                error: function (err) {
                    console.error(err);
                }
            });


        },
        error: function (err) {
            console.error(err);
        }
    });
});




function mandarSolicitud(id) {
    $.ajax({
        url: "http://localhost:8080/service/mandarSolicitud/" + id,
        success: function (res) {
            console.log(res);
            toastr['success'](
                    'Solicitud de amistad enviada '
                    )
            window.setTimeout(() => {
                window.location.reload()
            }, 1000)

        },
        error: function (err) {
            console.error(err);
        }
    });
}

function eliminarSolicitud(id) {
    $.ajax({
        url: "http://localhost:8080/service/cancelarSolicitud/" + id,
        success: function (res) {
            console.log(res);
            toastr['success'](
                    'Solicitud de amistad eliminada '
                    )
            window.setTimeout(() => {
                window.location.reload()
            }, 1000)

        },
        error: function (err) {
            console.error(err);
        }
    });
}


function aceptarSolicitud(id) {
    $.ajax({
        url: "http://localhost:8080/service/agregaAmigo/" + id,
        success: function (res) {
            console.log(res);
            toastr['success'](
                    'Amigo agregado '
                    )
            window.setTimeout(() => {
                window.location.reload()
            }, 1000)

        },
        error: function (err) {
            console.error(err);
        }
    });
}


function eliminarAmigo(id) {
    $.ajax({
        url: "http://localhost:8080/service/eliminaAmigo/" + id,
        success: function (res) {
            console.log(res);
            toastr['success'](
                    'Amigo eliminado'
                    )
            window.setTimeout(() => {
                window.location.reload()
            }, 1000)

        },
        error: function (err) {
            console.error(err);
        }
    });
}