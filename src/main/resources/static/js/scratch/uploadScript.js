$(document).ready(function () {
    //alert("loaded");
    // bind form submit event
    $("#avatars").change(function () {
        //alert("a");
        $("#buttonAvatar").prop("disabled", this.files.length == 0);
    });

    $("#file-upload-form").on("submit", function (e) {
        e.preventDefault();

        // use $.ajax() to upload file
        $.ajax({
            url: $("#file-upload-form").attr('action'),
            type: "POST",
            data: new FormData(this),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                toastr['success']("Imagen actualizada")
                window.setTimeout(() => {
                    window.location.reload()
                }, 2000)



            },
            error: function (err) {
                console.error(err);
            }
        });
    });
});
