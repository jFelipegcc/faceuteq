$(document).ready(function () {
    let token = $("meta[name='_csrf']").attr("content");
    let header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });



    $("#recuperaPass").on("submit", function (e) {

        e.preventDefault();

        let ps1 = $("#ps1").val();
        let ps2 = $("#ps2").val();
        let token_us = $("#token_us").val();
        let username = $("#username").text();
        //alert(username)

        if (ps1 === ps2) {
            $.ajax({
                url: "http://localhost:8080/files/recuperar",
                data: {
                    password: ps1,
                    username: username,
                    token_us: token_us
                },
                type: "POST",
                dataType: 'json',
                success: function (res) {
                    console.log(res);
                    toastr['success']("Contraseña actualizada")
                    //alert(res.message);
                    window.setTimeout(() => {
                        window.location.href = 'http://localhost:8080/login/'
                    }, 1000)
                },
                error: function (err) {
                     toastr['warning']("token incorrecto")
                    console.error(err);
                }
            });
        } else {
            toastr['warning']("Las contraseñas no coinciden")
        }


    });
});



