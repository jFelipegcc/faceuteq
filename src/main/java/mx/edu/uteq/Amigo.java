package mx.edu.uteq;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="amigo")
public class Amigo{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id_amigo;
    private String fecha_amigo;
    private Long usuario;
    private Long amigo;
    //@ManyToOne()
    // @JoinColumn(insertable=false, updatable=false,name = "usuario")
    //private Usuario miamigo;
    
}
