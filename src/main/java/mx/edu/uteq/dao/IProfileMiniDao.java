package mx.edu.uteq.dao;

import java.util.List;
import java.util.Optional;
import mx.edu.uteq.ProfileMini;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IProfileMiniDao extends JpaRepository<ProfileMini, Long> {

    @Query(value = "select id_usuario, carrera_us, nombre_us, gustos_us, apellido_us, username, puesto_us, (select imagen_avatar from avatar where usuario_avatar=:id  order by id_avatar desc limit 1)as avatar from usuario where id_usuario=:id", nativeQuery = true)
    ProfileMini miMiniPerfil(@Param("id") Long id);

    @Query(value = "select id_usuario, carrera_us, nombre_us, gustos_us, apellido_us, username, puesto_us, (select imagen_avatar from avatar where usuario_avatar=id_usuario  order by id_avatar desc limit 1)as avatar from usuario group by id_usuario", nativeQuery = true)
    List<ProfileMini> muchosMiniPerfil();

}
