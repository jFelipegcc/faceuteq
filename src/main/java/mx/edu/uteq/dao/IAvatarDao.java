package mx.edu.uteq.dao;

import java.util.List;
import java.util.Optional;
import mx.edu.uteq.Avatar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IAvatarDao extends JpaRepository<Avatar, Long>{

}