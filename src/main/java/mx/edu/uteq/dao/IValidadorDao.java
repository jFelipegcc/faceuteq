package mx.edu.uteq.dao;

import java.util.List;
import java.util.Optional;
import mx.edu.uteq.Rol;
import mx.edu.uteq.Validador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IValidadorDao extends JpaRepository<Validador, Long>{
      Validador findByUsername(String username);
   
}