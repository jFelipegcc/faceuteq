package mx.edu.uteq.dao;

import java.util.List;
import java.util.Optional;
import mx.edu.uteq.MisAmigos;
import mx.edu.uteq.MisSolis;
import mx.edu.uteq.ProfileMini;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IMisSolisDao extends JpaRepository<MisSolis, Long> {

    @Query(value = "select usuario.id_usuario, recibe_solicitud from usuario,solicitud where usuario.id_usuario=solicitud.envia_solicitud and usuario.id_usuario=:id", nativeQuery = true)
    List<MisSolis> misSolis(@Param("id") Long id);


}
