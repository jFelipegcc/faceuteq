package mx.edu.uteq.dao;

import java.util.List;
import java.util.Optional;
import mx.edu.uteq.Solicitud;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ISolicitudDao extends JpaRepository<Solicitud, Long>{
    
    @Query(value = "SELECT * FROM solicitud inner join usuario on usuario.id_usuario=recibe_solicitud WHERE envia_solicitud=:id", nativeQuery = true)
    List<Solicitud> misSolicitudesEnv(@Param("id") Long id);
    
    @Query(value = "SELECT * FROM solicitud inner join usuario on usuario.id_usuario=envia_solicitud WHERE recibe_solicitud=:id", nativeQuery = true)
    List<Solicitud> misSolicitudesRec(@Param("id") Long id);
    
    @Modifying
    @Query(value = "delete from solicitud where envia_solicitud=:suId and recibe_solicitud=:miId ", nativeQuery = true)
    void eliminaSolicitud1(@Param("miId") Long miId, @Param("suId") Long suId);
    
    @Modifying
    @Query(value = "delete from solicitud where envia_solicitud=:miId and recibe_solicitud=:suId ", nativeQuery = true)
    void eliminaSolicitud2(@Param("miId") Long miId, @Param("suId") Long suId);
}