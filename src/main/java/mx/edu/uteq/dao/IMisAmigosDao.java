package mx.edu.uteq.dao;

import java.util.List;
import java.util.Optional;
import mx.edu.uteq.MisAmigos;
import mx.edu.uteq.ProfileMini;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IMisAmigosDao extends JpaRepository<MisAmigos, Long> {

    @Query(value = "select usuario.id_usuario, amigo from usuario inner join amigo on amigo.usuario=usuario.id_usuario where usuario.id_usuario=:id", nativeQuery = true)
    List<MisAmigos> misAmigos(@Param("id") Long id);


}
