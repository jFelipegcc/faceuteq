package mx.edu.uteq.dao;

import java.util.List;
import java.util.Optional;
import mx.edu.uteq.Amigo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IAmigoDao extends JpaRepository<Amigo, Long>{
    //Solicitud findByEnvia_solicitud(Long id_usuario);
    //Solicitud findByRecibe_solicitud(Long id_usuario);
    
    @Query(value = "SELECT * FROM amigo inner join usuario on usuario.id_usuario=amigo WHERE amigo.usuario=:id", nativeQuery = true)
    List<Amigo> misAmigos(@Param("id") Long id);
    
    @Modifying
    @Query(value = "delete from amigo where usuario=:miId and amigo=:suId ", nativeQuery = true)
    void elimina1(@Param("miId") Long miId, @Param("suId") Long suId);
    
    @Modifying
    @Query(value = "delete from amigo where usuario=:suId and amigo=:miId ", nativeQuery = true)
    void elimina2(@Param("miId") Long miId, @Param("suId") Long suId);
    
}