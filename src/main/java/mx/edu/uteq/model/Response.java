/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mx.edu.uteq.model;

import lombok.Data;


@Data
public class Response {
    private String message;
    
    public Response(String message){
        this.message=message;
    }
}
