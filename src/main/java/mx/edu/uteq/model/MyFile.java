/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mx.edu.uteq.model;

import lombok.Data;


@Data
public class MyFile {
    private String name;
    private String url;
    
    public MyFile(String name, String url){
        this.name=name;
        this.url=url;
    }
}
