package mx.edu.uteq.security.service;

import java.util.ArrayList;
import lombok.extern.slf4j.Slf4j;
import mx.edu.uteq.dao.IUsuarioDao;
import mx.edu.uteq.Rol;
import mx.edu.uteq.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userDetailsService")
@Slf4j
public class UsuarioServiceSecurity implements UserDetailsService{

    @Autowired
    private IUsuarioDao usuarioDao;
    
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario usuario = usuarioDao.findByUsername(username);
        if (usuario==null){
            throw new UsernameNotFoundException(username);
        }
        ArrayList<GrantedAuthority> roles = new ArrayList<>();
        for (Rol rol : usuario.getRol()){
            roles.add(new SimpleGrantedAuthority(rol.getNombre()));
        }
        User user = new User(usuario.getUsername(), usuario.getPassword(), roles);
        return user;
    }  
}
