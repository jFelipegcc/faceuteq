package mx.edu.uteq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "mx.edu.uteq.dao")
public class FaceUteq {

	public static void main(String[] args) {
		SpringApplication.run(FaceUteq.class, args);
	}

}
