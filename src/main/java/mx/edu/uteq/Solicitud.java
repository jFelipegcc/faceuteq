package mx.edu.uteq;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="solicitud")
public class Solicitud{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id_solicitud;
    private String fecha_solicitud;
    private Long envia_solicitud;
    private Long recibe_solicitud;
    private Long estatus_solicitud;
    @ManyToOne
    @JoinColumn(insertable=false, updatable=false,name = "id_usuario")
    private Usuario envia;
    

}
