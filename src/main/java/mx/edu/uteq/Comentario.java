package mx.edu.uteq;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@Entity
@Table(name="comentario")
public class Comentario{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id_comment;
    private String contenido_comment;
    private Long publicacion_comment;
    private Long responde_comment;
    private Long usuario_comment;
    private Long estatus_comment;
    
    
}
