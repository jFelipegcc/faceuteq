

package mx.edu.uteq.service;

import java.util.List;
import mx.edu.uteq.ProfileMini;
import mx.edu.uteq.Rol;
import mx.edu.uteq.dao.IProfileMiniDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class IProfileMiniImp implements IProfileMiniService{
    
    @Autowired
    private IProfileMiniDao miniDao;

    @Override
    public ProfileMini miProfileMini(Long id) {
        return  miniDao.miMiniPerfil(id);
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<ProfileMini> muchosProfileMini(){
        return (List<ProfileMini>) miniDao.muchosMiniPerfil();
    }
   
    

    

    
    
}
