package mx.edu.uteq.service;

import java.util.List;
import mx.edu.uteq.Solicitud;
import mx.edu.uteq.dao.ISolicitudDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class ISolicitudImp implements ISolicitudService{
    
    @Autowired
    private ISolicitudDao SolicitudDao;
   
   
    @Override
    @Transactional   
    public void guardar(Solicitud solicitud) {
        SolicitudDao.save(solicitud);
    }
    
    
    @Override
    @Transactional(readOnly = true)
    public List<Solicitud> findByEnvia_solicitud(Long id) {
        return SolicitudDao.misSolicitudesEnv(id);
    }
    
    
    @Override
    @Transactional(readOnly = true)
    public List<Solicitud> findByRecibe_solicitud(Long id) {
        return (List<Solicitud>) SolicitudDao.misSolicitudesRec(id);
    }

    
    @Override
    @Transactional   
    public void eliminaSolicitud(Long miId, Long suId) {
        SolicitudDao.eliminaSolicitud1(miId, suId);
        SolicitudDao.eliminaSolicitud2(miId, suId);
    }

    
    
}
