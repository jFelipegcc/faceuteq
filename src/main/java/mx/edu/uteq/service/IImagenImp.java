

package mx.edu.uteq.service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import mx.edu.uteq.Avatar;
import mx.edu.uteq.Imagen;
import mx.edu.uteq.dao.IAvatarDao;
import mx.edu.uteq.dao.IImagenDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;


@Service
public class IImagenImp implements IImagenService{

    @Autowired
    private IImagenDao ImagenDao;
    private final Path rootFolder = Paths.get("src/main/resources/static/images/posts");
    
    @Override
    public void save(MultipartFile file,String cadena) throws Exception {
        Files.copy(file.getInputStream(), rootFolder.resolve(cadena));
    }
    
    @Override
    @Transactional   
    public void guardar(Imagen imagen) {
        ImagenDao.save(imagen);
    }
    
    
   
   
    
    
}
