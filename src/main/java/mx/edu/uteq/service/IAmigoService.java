
package mx.edu.uteq.service;


import java.util.List;
import mx.edu.uteq.Amigo;

public interface IAmigoService {
    public void guardar(Amigo amigo); 
    public List<Amigo> misAmigos(Long id);  
    public void eliminar(Long miId, Long suId);  
    
}
