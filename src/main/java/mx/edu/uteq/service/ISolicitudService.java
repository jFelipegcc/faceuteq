
package mx.edu.uteq.service;


import java.util.List;
import mx.edu.uteq.Solicitud;

public interface ISolicitudService {
    public void guardar(Solicitud solicitud); 
    public List<Solicitud> findByEnvia_solicitud(Long id); 
    public List<Solicitud> findByRecibe_solicitud(Long id);
    public void eliminaSolicitud(Long miId, Long suId); 
}
