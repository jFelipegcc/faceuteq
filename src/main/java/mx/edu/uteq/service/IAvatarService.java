package mx.edu.uteq.service;

import java.util.List;
import mx.edu.uteq.Avatar;
import org.springframework.web.multipart.MultipartFile;

public interface IAvatarService {

    public void save(MultipartFile file, String cadena) throws Exception;

    public void guardar(Avatar avatar);
     public List<Avatar> listaAvatars(); //Todos los avatars, admin
   

    //public void save(List<MultipartFile> files) throws Exception;

}
