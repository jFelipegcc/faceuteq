package mx.edu.uteq.service;

import java.util.List;
import mx.edu.uteq.MisAmigos;
import mx.edu.uteq.MisSolis;

public interface IMisSolisService {

    public List<MisSolis> misSolis(Long id);
    
}
