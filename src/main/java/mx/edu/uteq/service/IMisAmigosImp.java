

package mx.edu.uteq.service;

import java.util.List;
import mx.edu.uteq.MisAmigos;

import mx.edu.uteq.dao.IMisAmigosDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class IMisAmigosImp implements IMisAmigosService{
    
    @Autowired
    private IMisAmigosDao maDao;

    
    @Override
    @Transactional(readOnly = true)
    public List<MisAmigos> misAmigosi(Long id){
        return (List<MisAmigos>) maDao.misAmigos(id);
    }
   
    

    

    
    
}
