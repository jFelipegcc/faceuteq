
package mx.edu.uteq.service;


import java.util.List;
import mx.edu.uteq.Post;
import mx.edu.uteq.Usuario;

public interface IPostService {
    public List<Post> ListaPosts(); //Todos los usuarios, admin
    public Post guardar(Post post); //alta de usuario
    public Post findPostById(Long id_usuario); //Traer un usuario   
    
}
