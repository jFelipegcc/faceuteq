

package mx.edu.uteq.service;

import java.util.List;
import mx.edu.uteq.MisAmigos;
import mx.edu.uteq.MisSolis;

import mx.edu.uteq.dao.IMisAmigosDao;
import mx.edu.uteq.dao.IMisSolisDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class IMisSolisImp implements IMisSolisService{
    
    @Autowired
    private IMisSolisDao msDao;

    
    @Override
    @Transactional(readOnly = true)
    public List<MisSolis> misSolis(Long id){
        return (List<MisSolis>) msDao.misSolis(id);
    }
   
    

    

    
    
}
