package mx.edu.uteq.service;

import java.util.List;
import mx.edu.uteq.ProfileMini;

public interface IProfileMiniService {

    public ProfileMini miProfileMini(Long id);
    public List<ProfileMini> muchosProfileMini();
}
