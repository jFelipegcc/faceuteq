package mx.edu.uteq.service;

import java.util.List;
import mx.edu.uteq.MisAmigos;

public interface IMisAmigosService {

    public List<MisAmigos> misAmigosi(Long id);
    
}
