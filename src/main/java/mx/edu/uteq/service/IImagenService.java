package mx.edu.uteq.service;

import java.util.List;
import mx.edu.uteq.Avatar;
import mx.edu.uteq.Imagen;
import org.springframework.web.multipart.MultipartFile;

public interface IImagenService {

    public void guardar(Imagen imagen);
    public void save(MultipartFile file, String cadena) throws Exception;
    
     

    //public void save(List<MultipartFile> files) throws Exception;

}
