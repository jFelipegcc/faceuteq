
package mx.edu.uteq.service;


import java.util.List;
import mx.edu.uteq.Usuario;

public interface IUsuarioService {
    public List<Usuario> ListaUsuarios(); //Todos los usuarios, admin
    public void guardar(Usuario usuario); //alta de usuario
    public Usuario findUser(Usuario usuario); //Traer un usuario   
    public Usuario findUserById(Long id_usuario); //Traer un usuario   
    public Usuario findUserByUsername(String username); //Traer un usuario   
    
}
