

package mx.edu.uteq.service;

import java.util.List;
import mx.edu.uteq.Rol;
import mx.edu.uteq.dao.IRolDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class IRolImp implements IRolService{
    
    @Autowired
    private IRolDao RolDao;
   
    @Override
    @Transactional(readOnly = true)
    public List<Rol> ListaRoles(){
        return (List<Rol>) RolDao.findAll();
    }

    @Override
    @Transactional   
    public void guardar(Rol rol) {
        RolDao.save(rol);
    }
    
    @Override
    @Transactional(readOnly = true)
    public Rol findUserByNombre(String username) {
        return RolDao.findByNombre(username);
    }

    

    
    
}
