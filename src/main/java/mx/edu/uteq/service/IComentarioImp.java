package mx.edu.uteq.service;

import java.util.List;
import mx.edu.uteq.Amigo;
import mx.edu.uteq.Comentario;
import mx.edu.uteq.dao.IAmigoDao;
import mx.edu.uteq.dao.IComentarioDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class IComentarioImp implements IComentarioService{
    
    @Autowired
    private IComentarioDao comentDao;
   
   
    @Override
    @Transactional   
    public void guardar(Comentario comment) {
        comentDao.save(comment);
    }
    
    
    
}
