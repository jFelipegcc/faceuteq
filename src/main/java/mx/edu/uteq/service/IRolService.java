
package mx.edu.uteq.service;


import java.util.List;
import mx.edu.uteq.Rol;

public interface IRolService {
    public List<Rol> ListaRoles(); //Todos los roles
    public void guardar(Rol rol); //alta de rol
    public Rol findUserByNombre(String username); //Traer un usuario   
    }
