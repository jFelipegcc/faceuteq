package mx.edu.uteq.service;

import java.util.List;
import mx.edu.uteq.Amigo;
import mx.edu.uteq.dao.IAmigoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class IAmigoImp implements IAmigoService{
    
    @Autowired
    private IAmigoDao AmigoDao;
   
   
    @Override
    @Transactional   
    public void guardar(Amigo amigo) {
        AmigoDao.save(amigo);
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<Amigo> misAmigos(Long id) {
        return (List<Amigo>) AmigoDao.misAmigos(id);
    }
    
    @Override
    @Transactional   
    public void eliminar(Long miId, Long suId) {
        AmigoDao.elimina1(miId, suId);
        AmigoDao.elimina2(miId, suId);
    }

    

    
    
}
