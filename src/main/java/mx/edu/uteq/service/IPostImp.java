

package mx.edu.uteq.service;

import java.util.List;
import mx.edu.uteq.Post;
import mx.edu.uteq.Usuario;
import mx.edu.uteq.dao.IPostDao;
import mx.edu.uteq.dao.IUsuarioDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class IPostImp implements IPostService{
    
    @Autowired
    private IPostDao postDao;
   
    @Override
    @Transactional(readOnly = true)
    public List<Post> ListaPosts(){
        return (List<Post>) postDao.findAll();
    }

    @Override
    @Transactional   
    public Post guardar(Post post) {
       return postDao.save(post);
    }

    @Override
    @Transactional(readOnly = true)
    public Post findPostById(Long id_usuario) {
        return postDao.findByAutor(id_usuario);
    }
    

    
    
}
