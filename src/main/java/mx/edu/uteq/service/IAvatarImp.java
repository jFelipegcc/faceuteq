

package mx.edu.uteq.service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import mx.edu.uteq.Avatar;
import mx.edu.uteq.dao.IAvatarDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;


@Service
public class IAvatarImp implements IAvatarService{

    @Autowired
    private IAvatarDao AvatarDao;
    private final Path rootFolder = Paths.get("src/main/resources/static/images/avatars");
    
    @Override
    public void save(MultipartFile file,String cadena) throws Exception {
        Files.copy(file.getInputStream(), rootFolder.resolve(cadena));
    }
    /*
    @Override
    public void save(List<MultipartFile> files) throws Exception {
        for(MultipartFile file:files){
            save(file);
        }
    }
    */
    @Override
    @Transactional   
    public void guardar(Avatar avatar) {
        AvatarDao.save(avatar);
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<Avatar> listaAvatars(){
        return (List<Avatar>) AvatarDao.findAll();
    }
   
   
    
    
}
