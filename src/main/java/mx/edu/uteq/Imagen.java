package mx.edu.uteq;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@Entity
@Table(name="imagen")
public class Imagen{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id_img;
    private String imagen_img;
    private String fecha_img;
    private Long publicacion_img;
    private Long estatus_img;
}
