

package mx.edu.uteq.commons;

import mx.edu.uteq.model.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;


@ControllerAdvice
public class FileUploadExceptionAdvice {
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<Response> handleMaxSizeException(){
        String msg = "Archivo muy grande";
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Response(msg));
    }
    /*
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Response> handleException(){
        String msg = "Ocurrió un error";
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Response(msg));
    }
*/
}
