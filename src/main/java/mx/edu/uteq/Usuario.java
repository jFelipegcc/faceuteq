/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mx.edu.uteq;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="usuario")
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id_usuario;
    private String username;
    private String password;
    private String nombre_us;
    private String apellido_us;
    private String telefono_us;
    private String fc_nac_us;
    private Long genero_us;
    private Long puesto_us;
    private String carrera_us;
    private String formacion_us;
    private String trabajo_us;
    private String empresa_us;
    private String lugar_nac_us;
    private String sentimental_us;
    private String gustos_us;
    private Long vpublicacion_us; //Visibilidad
    private String token_us; //Token para recuperar contraseña
    private String fc_token_us; //Fecha de creacion del token
    private Long estatus_us;
    @OneToMany
    @JoinColumn(insertable=false, updatable=false,name = "id_usuario")
    private List<Rol> rol;
    @OneToMany()
    @JoinColumn(insertable=false, updatable=false,name = "usuario")//foranea de la otra tabla
    private List<Amigo> amigos;
    @OneToMany()
    @JoinColumn(insertable=false, updatable=false,name = "usuario_avatar")//foranea de la otra tabla
    private List<Avatar> avatars;

    

    
}
