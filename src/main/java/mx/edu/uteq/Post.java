package mx.edu.uteq;

import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@Entity
@Table(name="publicacion")
public class Post{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id_publicacion;
    private String titulo_publicacion;
    private String descripcion_publicacion;
    private Long visibilidad_publicacion;
    private Long autor;
    private Long estatus_publicacion;
    @OneToMany
    @JoinColumn(insertable=false, updatable=false,name = "publicacion_img")
    private List<Imagen> imagenes;
    @OneToMany
    @JoinColumn(insertable=false, updatable=false,name = "publicacion_comment")
    private List<Comentario> comentarios;
    
}
