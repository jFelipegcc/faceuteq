package mx.edu.uteq.controller;

import java.io.IOException;
import java.util.List;
import javax.validation.Valid;
import mx.edu.uteq.Usuario;
import mx.edu.uteq.model.Response;
import mx.edu.uteq.service.IAvatarService;
import mx.edu.uteq.service.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class PostCtrl {

        
    @Autowired
    IAvatarService avatarService;

    @Autowired
    IUsuarioService usuarioService;


    //alta de posts
    @GetMapping("/post/alta") //solo queremos  peticiones get
    public String agregarUsuario(Usuario usuario) {
        return "upPost";

    }

    //------------Fin alta de post----------------
    
   @GetMapping("/avatar/subir") //solo queremos  peticiones get
    public String page(Model model) {
        return "uploadAvatar";
    }
    
    
}
