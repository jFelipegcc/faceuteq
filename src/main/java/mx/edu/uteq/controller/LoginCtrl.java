package mx.edu.uteq.controller;

import java.util.List;
import javax.validation.Valid;
import mx.edu.uteq.Rol;
import mx.edu.uteq.Usuario;
import mx.edu.uteq.service.IRolService;
import mx.edu.uteq.service.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoginCtrl {

    @GetMapping("/login")
    public String loginPage() {
        return "login";
    }
}
