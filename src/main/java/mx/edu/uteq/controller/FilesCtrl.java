/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/springframework/RestController.java to edit this template
 */
package mx.edu.uteq.controller;

import static java.lang.Math.random;
import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import java.util.Random;
import mx.edu.uteq.Avatar;
import mx.edu.uteq.Comentario;
import mx.edu.uteq.Imagen;
import mx.edu.uteq.MisAmigos;
import mx.edu.uteq.MisSolis;
import mx.edu.uteq.Post;
import mx.edu.uteq.ProfileMini;
import mx.edu.uteq.Rol;
import mx.edu.uteq.Solicitud;
import mx.edu.uteq.Usuario;
import mx.edu.uteq.Validador;
import mx.edu.uteq.dao.IValidadorDao;
import mx.edu.uteq.email.SendEmailService;
import mx.edu.uteq.model.Response;
import mx.edu.uteq.service.IAmigoService;
import mx.edu.uteq.service.IAvatarService;
import mx.edu.uteq.service.IComentarioService;
import mx.edu.uteq.service.IImagenService;
import mx.edu.uteq.service.IMisAmigosService;
import mx.edu.uteq.service.IMisSolisService;
import mx.edu.uteq.service.IPostService;
import mx.edu.uteq.service.IProfileMiniService;
import mx.edu.uteq.service.IRolService;
import mx.edu.uteq.service.ISolicitudService;
import mx.edu.uteq.service.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/files")
public class FilesCtrl {

    @Autowired
    SendEmailService ses;
    @Autowired
    IAvatarService avatarService;

    @Autowired
    IValidadorDao validadorDao;

    @Autowired
    IComentarioService comentarioService;

    @Autowired
    IUsuarioService usuarioService;

    @Autowired
    IRolService rolService;

    @Autowired
    ISolicitudService solicitudService;

    @Autowired
    IAmigoService amigoService;

    @Autowired
    IProfileMiniService miniService;

    @Autowired
    IMisAmigosService maService;

    @Autowired
    IMisSolisService msService;

    @Autowired
    IPostService postService;

    @Autowired
    IImagenService imgService;

    @PostMapping("/subir")
    public ResponseEntity<Response> uploadAvatar(Principal principal, @RequestParam("avatars") List<MultipartFile> avatars) throws Exception {
        int i = 0;
        for (MultipartFile avatar : avatars) {
            i++;
            Avatar pic = new Avatar();
            pic.setEstatus_avatar(Long.valueOf(0));

            Usuario usuario = usuarioService.findUserByUsername(principal.getName());

            DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("HH_mm_ss");
            String hoy2 = dtf2.format(LocalDateTime.now());

            String extension = avatar.getOriginalFilename().substring(avatar.getOriginalFilename().lastIndexOf(".") + 1);

            String cadena = i + "_" + hoy2 + "_" + usuario.getId_usuario() + "." + extension;

            pic.setImagen_avatar(cadena);

            pic.setUsuario_avatar(usuario.getId_usuario());
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            String hoy = dtf.format(LocalDateTime.now());

            pic.setFecha_avatar(hoy);
            avatarService.guardar(pic);
            avatarService.save(avatar, cadena);
        }

        return ResponseEntity.status(HttpStatus.OK).body(new Response("Avatar guardado exitosamente"));
    }

    @RequestMapping(value = "/publicar", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public ResponseEntity<Response> uploadPost(Principal principal, @RequestPart("pozt") Post post,
            @RequestPart("imagenes") List<MultipartFile> imagenes) throws Exception {
        post.setAutor(usuarioService.findUserByUsername(principal.getName()).getId_usuario());
        post.setEstatus_publicacion(Long.valueOf("1"));
        Post newPost = postService.guardar(post);

        int i = 0;
        for (MultipartFile imagen : imagenes) {
            i++;
            Imagen img = new Imagen();

            // Usuario usuario = usuarioService.findUserByUsername(principal.getName());
            DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("HH_mm_ss");
            String hoy2 = dtf2.format(LocalDateTime.now());

            String extension = imagen.getOriginalFilename().substring(imagen.getOriginalFilename().lastIndexOf(".") + 1);

            String cadena = i + "POST_" + hoy2 + "_" + post.getAutor() + "." + extension;

            img.setImagen_img(cadena);
            img.setPublicacion_img(newPost.getId_publicacion());
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            String hoy = dtf.format(LocalDateTime.now());
            img.setEstatus_img(Long.valueOf("1"));
            img.setFecha_img(hoy);
            imgService.save(imagen, cadena);
            imgService.guardar(img);
        }
        return ResponseEntity.status(HttpStatus.OK).body(new Response("Post guardado exitosamente"));

    }

    @PostMapping("/guardaUsuario")
    public ResponseEntity<Response> crearUsuario(Usuario usuario, Validador val) {

        Validador trueVal = validadorDao.findByUsername(usuario.getUsername());
        if (trueVal.getToken().equals(val.getToken())) {
            validadorDao.deleteAll();
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            String encPass = encoder.encode(usuario.getPassword());
            usuario.setPassword(encPass);
            usuario.setEstatus_us(Long.valueOf(0));
            usuarioService.guardar(usuario);
            Rol nuevoRol = new Rol();
            nuevoRol.setNombre("ROLE_USER");
            nuevoRol.setId_usuario(usuario.getId_usuario());
            rolService.guardar(nuevoRol);
            return ResponseEntity.status(HttpStatus.OK).body(new Response("Usuario guardado exitosamente"));
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response("Token incorrecto"));

        }
    }

    @PostMapping("/recuperar")
    public ResponseEntity<Response> recuperar(Usuario usuario) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encPass = encoder.encode(usuario.getPassword());

        Usuario realUser = usuarioService.findUserByUsername(usuario.getUsername());
        if (realUser.getToken_us().equals(usuario.getToken_us())) {
            realUser.setPassword(encPass);
            realUser.setToken_us(null);
            usuarioService.guardar(realUser);
            return ResponseEntity.status(HttpStatus.OK).body(new Response("Usuario actualizado exitosamente"));

        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response("Usuario no actualizado exitosamente"));

        }

    }

    @PostMapping("/solicitaToken")
    public ResponseEntity<Response> solicitar(Usuario usuario) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        String token = random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        Validador val = new Validador();
        val.setUsername(usuario.getUsername());
        val.setToken(token);
        validadorDao.save(val);
        String cuerpo = "Tu token es el siguiente: " + token;
        ses.sendEmail(usuario.getUsername(), cuerpo, "Token para tu registro");

        return ResponseEntity.status(HttpStatus.OK).body(new Response("Token enviado satisfactoriamnete"));

    }

    @PostMapping("/guardaComentario")
    public ResponseEntity<Response> crearComentario(Principal principal, Comentario comentario) {
        comentario.setUsuario_comment(usuarioService.findUserByUsername(principal.getName()).getId_usuario());
        comentario.setEstatus_comment(Long.valueOf("1"));
        comentarioService.guardar(comentario);
        return ResponseEntity.status(HttpStatus.OK).body(new Response("Comentario guardado exitosamente"));

    }

    @PostMapping("/getMinis")
    public List<ProfileMini> todosMinis() {
        List<ProfileMini> lista;
        lista = miniService.muchosProfileMini();
        return lista;
    }

    @GetMapping("/getMiMini/{id}")
    public ProfileMini miMini(@PathVariable Long id) {
        ProfileMini lista = miniService.miProfileMini(id);
        return lista;
    }

    @GetMapping("/getSuInfo/{id}")
    public Usuario suInfo(@PathVariable Long id) {
        Usuario lista = usuarioService.findUserById(id);
        return lista;
    }

    @PostMapping("/getAmigos")
    public List<MisAmigos> misAmigos(Principal principal) {
        String nombre = principal.getName();
        List<MisAmigos> lista;
        lista = maService.misAmigosi(
                usuarioService.findUserByUsername(nombre).getId_usuario());
        return lista;
    }

    @PostMapping("/getSolis")
    public List<Solicitud> misSolis(Principal principal) {
        String nombre = principal.getName();
        List<Solicitud> lista;
        lista = solicitudService.findByEnvia_solicitud(usuarioService.findUserByUsername(nombre).getId_usuario());
        return lista;
    }

    @PostMapping("/getSolisR")
    public List<Solicitud> misSolisR(Principal principal) {
        String nombre = principal.getName();
        List<Solicitud> lista;
        lista = solicitudService.findByRecibe_solicitud(usuarioService.findUserByUsername(nombre).getId_usuario());
        return lista;
    }

    @PostMapping("/miInfo")
    public Usuario miInfo(Principal principal) {
        String nombre = principal.getName();
        Usuario lista;
        lista = usuarioService.findUserByUsername(nombre);
        return lista;
    }

    @PostMapping("/posts")
    public List<Post> posts() {
        List<Post> posts = postService.ListaPosts();
        return posts;
    }

}
