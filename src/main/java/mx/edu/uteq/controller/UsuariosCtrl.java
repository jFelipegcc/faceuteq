package mx.edu.uteq.controller;

import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Random;
import javax.validation.Valid;
import mx.edu.uteq.Amigo;
import mx.edu.uteq.Post;
import mx.edu.uteq.ProfileMini;
import mx.edu.uteq.Rol;
import mx.edu.uteq.Solicitud;
import mx.edu.uteq.Usuario;
import mx.edu.uteq.email.SendEmailService;
import mx.edu.uteq.service.IAmigoService;
import mx.edu.uteq.service.IPostService;
import mx.edu.uteq.service.IProfileMiniService;
import mx.edu.uteq.service.IRolService;
import mx.edu.uteq.service.ISolicitudService;
import mx.edu.uteq.service.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UsuariosCtrl {

    @Autowired
    IUsuarioService usuarioService;

    @Autowired
    IRolService rolService;

    @Autowired
    ISolicitudService solicitudService;

    @Autowired
    IAmigoService amigoService;

    @Autowired
    IProfileMiniService miniService;

    @Autowired
    IPostService postService;
    @Autowired
    SendEmailService ses;

    @GetMapping("/") //solo queremos  peticiones get
    public String page(Model model) {
        // model.addAttribute("attribute", "value");
        return "index";
    }

    //pagina de usuarios
    @GetMapping("/public/registro")
    public String agregarUsuario() {
        return "registro";

    }

    @GetMapping("/public/recupera")
    public String recuperaPass() {
        return "recupera_pass1";

    }

    @PostMapping("/public/solicita_cambio")
    public String recuperaPassSend(Model model, Usuario usuario) {
        //System.out.println(usuario.getUsername());
        Usuario pruebaUsuario = usuarioService.findUserByUsername(usuario.getUsername());
        if (pruebaUsuario.getUsername().isEmpty()) {
            return "recupera_pass1";
        } else {
            int leftLimit = 97; // letter 'a'
            int rightLimit = 122; // letter 'z'
            int targetStringLength = 10;
            Random random = new Random();

            String token = random.ints(leftLimit, rightLimit + 1)
                    .limit(targetStringLength)
                    .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                    .toString();

            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            String hoy = dtf.format(LocalDateTime.now());

            pruebaUsuario.setFc_token_us(hoy);
            pruebaUsuario.setToken_us(token);
            usuarioService.guardar(pruebaUsuario);
            
            model.addAttribute("usuario", pruebaUsuario.getUsername());

            String cuerpo = "Tu token es el siguiente: " + token;
            ses.sendEmail(usuario.getUsername(), cuerpo, "Recupera tu contraseña");

        }
        return "recupera_pass2";

    }

    @GetMapping("/private/home")
    public String homeUsuario(Model model, Usuario usuario, Principal principal) {
        if (principal != null) {
            String logeado = principal.getName();
            usuario = usuarioService.findUserByUsername(logeado);
            ProfileMini mini = miniService.miProfileMini(usuario.getId_usuario());
            model.addAttribute("usuario", usuario);
            model.addAttribute("logeado", logeado);
            model.addAttribute("mini", mini);
            return "public_profile";
        } else {
            return "login";
        }
    }

    @GetMapping("/private/posts")
    public String postsUsuario(Model model, Usuario usuario, Principal principal) {
        if (principal != null) {
            String logeado = principal.getName();
            usuario = usuarioService.findUserByUsername(logeado);
            ProfileMini mini = miniService.miProfileMini(usuario.getId_usuario());
            List<Post> post = postService.ListaPosts();
            model.addAttribute("usuario", usuario);
            model.addAttribute("logeado", logeado);
            model.addAttribute("posts", post);
            model.addAttribute("mini", mini);
            return "post_view";
        } else {
            return "login";
        }
    }

    @GetMapping("/private/configuracion")
    public String configuracionUsuario(Model model, Usuario usuario, Principal principal) {
        if (principal != null) {
            String logeado = principal.getName();
            usuario = usuarioService.findUserByUsername(logeado);
            ProfileMini mini = miniService.miProfileMini(usuario.getId_usuario());
            model.addAttribute("usuario", usuario);
            model.addAttribute("logeado", logeado);
            model.addAttribute("mini", mini);
            return "settings_form";
        } else {
            return "login";
        }

    }

    @GetMapping("/private/subirPost")
    public String upPost(Model model, Usuario usuario, Principal principal) {
        if (principal != null) {
            String logeado = principal.getName();
            usuario = usuarioService.findUserByUsername(logeado);
            ProfileMini mini = miniService.miProfileMini(usuario.getId_usuario());
            model.addAttribute("usuario", usuario);
            model.addAttribute("logeado", logeado);
            model.addAttribute("mini", mini);
            return "form_post";
        } else {
            return "login";
        }

    }

    @GetMapping("/private/usuarios")
    public String findFriends(Model model, Usuario usuario, Principal principal) {
        if (principal != null) {
            String logeado = principal.getName();
            usuario = usuarioService.findUserByUsername(logeado);
            ProfileMini mini = miniService.miProfileMini(usuario.getId_usuario());
            List<ProfileMini> usuarios;
            usuarios = miniService.muchosProfileMini();
            model.addAttribute("usuarios", usuarios);
            model.addAttribute("usuario", usuario);
            model.addAttribute("logeado", logeado);
            model.addAttribute("mini", mini);
            return "find_friends";
        } else {
            return "login";
        }

    }

    //@PostMapping("/service/guardaUsuario")
    //public String crearUsuario(Usuario usuario) {
    //BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    //String encPass = encoder.encode(usuario.getPassword());
    //usuario.setPassword(encPass);
    //usuario.setEstatus_us(Long.valueOf(0));
    //usuarioService.guardar(usuario);
    //Rol nuevoRol = new Rol();
    //nuevoRol.setNombre("ROLE_USER");
    //nuevoRol.setId_usuario(usuario.getId_usuario());
    // rolService.guardar(nuevoRol);
    //   return "redirect:/usuario/alta";
    // }
    //------------Fin alta de usuario----------------
    //Consultar de usuarios
    @GetMapping("/usuario/ver") //solo queremos  peticiones get
    public String consultarUsuario(Model model, Usuario usuario, Principal principal) {
        if (principal != null) {
            String logeado = principal.getName();
            // Long id_prueba = Long.valueOf("5");

            usuario = usuarioService.findUserByUsername(logeado);
            model.addAttribute("datos", usuario);
            model.addAttribute("logeado", logeado);
            return "verUsuario";
        } else {
            return "login";
        }
    }

    @GetMapping("/usuario/todos") //solo queremos  peticiones get
    public String consultarTodosUsuario(Model model, Usuario usuario, Principal principal) {
        if (principal != null) {
            String logeado = principal.getName();
            // Long id_prueba = Long.valueOf("5");
            List<Usuario> usuarios;
            usuarios = usuarioService.ListaUsuarios();
            model.addAttribute("datos", usuarios);
            model.addAttribute("logeado", logeado);
            return "verUsuario";

        } else {
            return "login";
        }
    }

    @GetMapping("/public/test") //solo queremos  peticiones get
    public void test() {
        ses.sendEmail("jfelipe.garc@gmail.com", "Este es el cuerpo", "Prueba");
    }

    //------------fin consultar de usuario----------------
    //editar usuarios
    //@GetMapping("/usuario/editar") //solo queremos  peticiones get
    //public String editarUsuario(Usuario usuario, Model model, Principal principal) {
    //String logeado = principal.getName();
    //Long id_prueba = Long.valueOf("5");
    //usuario = usuarioService.findUserByUsername(logeado);
    // model.addAttribute("usuario", usuario);
    //  return "editUsuario";
    //}
    @PostMapping("/service/updateUsuario")
    public String updateUser(Usuario usuario) {
        usuario.setPassword(usuarioService.findUserById(usuario.getId_usuario()).getPassword());
        usuario.setEstatus_us(Long.valueOf(0));
        usuarioService.guardar(usuario);
        return "redirect:/private/configuracion";
    }
    //------------Fin editar de usuario----------------

    //--------------Solicitud de amistad--------------------------
    @GetMapping("/private/verUsuario/{id_usuario}")
    public String verUnUsuario(Usuario usuario, Model model, @PathVariable Long id_usuario, Principal principal) {
        usuario = usuarioService.findUserById(id_usuario);
        ProfileMini mini = miniService.miProfileMini(id_usuario);

        Usuario usuarioYo = usuarioService.findUserByUsername(principal.getName());
        ProfileMini miniYo = miniService.miProfileMini(usuarioYo.getId_usuario());

        model.addAttribute("usuarioe", usuario);
        model.addAttribute("minie", mini);

        model.addAttribute("usuario", usuarioYo);
        model.addAttribute("mini", miniYo);
        return "profile";
    }

    @GetMapping("/service/mandarSolicitud/{id_usuario}")
    public String agregarUsuario(Solicitud solicitud, @PathVariable Long id_usuario, Principal principal) {
        Usuario envia = usuarioService.findUserByUsername(principal.getName());
        solicitud.setEnvia_solicitud(envia.getId_usuario());
        solicitud.setRecibe_solicitud(id_usuario);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        String hoy = dtf.format(LocalDateTime.now());
        solicitud.setFecha_solicitud(hoy);
        solicitud.setEstatus_solicitud(Long.valueOf(0));
        solicitudService.guardar(solicitud);
        return "redirect:/private/usuarios";
    }

    @GetMapping("/service/cancelarSolicitud/{id_usuario}")
    public String cancelarUsuario(Solicitud solicitud, @PathVariable Long id_usuario, Principal principal) {
        String logeado = principal.getName();
        Long miId = usuarioService.findUserByUsername(logeado).getId_usuario();
        solicitudService.eliminaSolicitud(miId, id_usuario);
        return "redirect:/private/usuarios";
    }

    /*@GetMapping("/amigos/solicitudes") //solo queremos  peticiones get
    public String solicitudes(Model model, Solicitud solicitud, Principal principal) {
        if (principal != null) {
            String logeado = principal.getName();
            // Long id_prueba = Long.valueOf("5");
            Long miId = usuarioService.findUserByUsername(logeado).getId_usuario();
            List<Solicitud> enviadas = solicitudService.findByEnvia_solicitud(miId);
            List<Solicitud> recibidas = solicitudService.findByRecibe_solicitud(miId);
            model.addAttribute("enviadas", enviadas);
            model.addAttribute("recibidas", recibidas);
            model.addAttribute("logeado", logeado);
            return "misSolicitudesEnv";

        } else {
            return "login";
        }
    } */
    @GetMapping("/amigos/misAmigos") //solo queremos  peticiones get
    public String amigos(Model model, Solicitud solicitud, Principal principal) {
        if (principal != null) {
            String logeado = principal.getName();
            // Long id_prueba = Long.valueOf("5");
            Long miId = usuarioService.findUserByUsername(logeado).getId_usuario();
            List<Amigo> misAmigos = amigoService.misAmigos(miId);
            model.addAttribute("misAmigos", misAmigos);
            model.addAttribute("logeado", logeado);
            return "amigos";

        } else {
            return "login";
        }
    }

    //Aceptar solicitud
    @GetMapping("/service/agregaAmigo/{id_usuario}") //solo queremos  peticiones get
    public String addFriend(Model model, Amigo amigo, Principal principal, @PathVariable Long id_usuario) {
        if (principal != null) {
            String logeado = principal.getName();
            Long miId = usuarioService.findUserByUsername(logeado).getId_usuario();
            solicitudService.eliminaSolicitud(miId, id_usuario);
            amigo.setAmigo(id_usuario);
            amigo.setUsuario(miId);
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            String hoy = dtf.format(LocalDateTime.now());
            amigo.setFecha_amigo(hoy);
            amigoService.guardar(amigo);
            Amigo amigo2 = new Amigo();
            amigo2.setAmigo(miId);
            amigo2.setUsuario(id_usuario);
            amigo2.setFecha_amigo(hoy);
            amigoService.guardar(amigo2);

            return "redirect:/private/usuarios";

        } else {
            return "login";
        }
    }

    @GetMapping("/service/eliminaAmigo/{id_usuario}") //solo queremos  peticiones get
    public String killFriend(Model model, Amigo amigo, Principal principal, @PathVariable Long id_usuario) {
        if (principal != null) {
            String logeado = principal.getName();
            Long miId = usuarioService.findUserByUsername(logeado).getId_usuario();
            amigoService.eliminar(miId, id_usuario);
            return "redirect:/private/usuarios";

        } else {
            return "login";
        }
    }

}
