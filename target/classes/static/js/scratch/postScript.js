$(document).ready(function () {
    let token = $("meta[name='_csrf']").attr("content");
    let header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });


    load_posts();

    $("#postForm").on("submit", function (e) {
        e.preventDefault();
        // use $.ajax() to upload file
        var formData = new FormData();
        for (i = 0; i <= $("#imagenes").prop("files").length; i++) {

            formData.append("imagenes", $("#imagenes").prop("files")[i]);
        }
        formData.append('pozt', new Blob([JSON.stringify({
                "titulo_publicacion": document.getElementById("titulo_publicacion").value,
                "descripcion_publicacion": document.getElementById("descripcion_publicacion").value,
                "visibilidad_publicacion": document.getElementById("visibilidad_publicacion").value
            })], {
            type: "application/json"
        }))

        $.ajax({
            url: 'http://localhost:8080/files/publicar',
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'json',
            enctype: 'multipart/form-data',
            success: function (res) {
                toastr['success']("Post creado")
                console.log(res);
            },
            error: function (err) {
                console.error(err);
            }
        });
    });



}





);

function a(textarea, id_post) {
    let texto = $('#' + textarea.id).val()
    //alert(texto);
    $.ajax({
        url: 'http://localhost:8080/files/guardaComentario',
        type: "POST",
        data: {
            "contenido_comment": texto,
            "publicacion_comment": id_post,
        },
        dataType: 'json',
        success: function (res) {
            toastr['success']("Comentario creado exitosamente")
            console.log(res);
            load_posts()
        },
        error: function (err) {
            console.error(err);
        }
    });
}


function load_posts() {

    let miId;
    $.ajax({
        url: "http://localhost:8080/files/miInfo",
        type: "POST",
        dataType: 'json',
        success: function (yo) {
            miId = yo.id_usuario;
        },
        error: function (err) {
            //console.error(err);
        }
    });

    $("#divPosts").html("");

    $.ajax({
        url: "http://localhost:8080/files/posts",
        type: "POST",
        dataType: 'json',
        success: function (posts) {
            console.log(posts);
            let cont = "";
            posts.forEach(function (post) {
                $.ajax({
                    url: 'http://localhost:8080/files/getSuInfo/' + post.autor,
                    type: "GET",
                    dataType: 'json',
                    success: function (user) {
                        let permiso = 0;
                        let isFriend=0;
                        console.log("user", user.amigos);
                        user.amigos.forEach(function (amigo) {
                            if (amigo.amigo == miId) {
                                isFriend++;
                            }
                        })
                        if(post.visibilidad_publicacion==0){
                            if(isFriend>0){
                                permiso=1;
                            }
                        }else{
                            permiso = 1;
                        }
                        console.log(permiso);
                        if (permiso==1) {
                            let cont = "";
                            cont += '<div class="card bg-gray-dark rounded">'
                            cont += '<div class="card-body">'
                            cont += '<h5 class="card-title">' + post.titulo_publicacion + '</h5>'
                            cont += ' <div class="">'

                            cont += '<div id="carouselExampleControls' + post.id_publicacion + '" class="carousel slide" data-ride="carousel">'
                            cont += '<div class="carousel-inner">'
                            let h = 0;
                            post.imagenes.forEach(function (img) {
                                if (h == 0) {
                                    cont += '<div class="carousel-item active">'
                                } else {
                                    cont += '<div class="carousel-item">'

                                }
                                h++;
                                cont += '<img class="d-block w-100" src="/images/posts/' + img.imagen_img + '" alt="First slide">'
                                cont += '</div>'
                            })
                            cont += '</div>'
                            cont += '<a class="carousel-control-prev" href="#carouselExampleControls' + post.id_publicacion + '" role="button" data-slide="prev">'
                            cont += '<span class="carousel-control-prev-icon" aria-hidden="true"></span>'
                            cont += '<span class="sr-only">Previous</span>'
                            cont += '</a>'
                            cont += '<a class="carousel-control-next" href="#carouselExampleControls' + post.id_publicacion + '" role="button" data-slide="next">'
                            cont += '<span class="carousel-control-next-icon" aria-hidden="true"></span>'
                            cont += '<span class="sr-only">Next</span>'
                            cont += '</a>'
                            cont += '</div>'

                            cont += '<div class="d-flex py-4">'
                            cont += '<div class="preview-list w-100">'
                            cont += '<h6 class="text-muted">' + post.descripcion_publicacion + '</h6>'
                            cont += '<div id="com_' + post.id_publicacion + '">'
                            cont += '</div>'
                            //Comentario
                            post.comentarios.forEach(function (comentario) {
                                let mini
                                let contc = "";
                                $.ajax({
                                    url: 'http://localhost:8080/files/getMiMini/' + comentario.usuario_comment,
                                    type: "GET",
                                    dataType: 'json',
                                    success: function (res) {
                                        //  alert("s");
                                        console.log(res);
                                        //load_posts()
                                        mini = res;
                                        //  if (contc != undefined) {
                                        contc += '<div class="preview-item p-0">'
                                        contc += '<div class="preview-thumbnail">'
                                        contc += '<img src="/images/avatars/' + mini.avatar + '" class="rounded-circle" alt="">'
                                        contc += '</div>'
                                        contc += '<div class="preview-item-content d-flex flex-grow">'
                                        contc += '<div class="flex-grow">'
                                        contc += '<div class="d-flex d-md-block d-xl-flex justify-content-between">'
                                        contc += '<h6 class="preview-subject">' + mini.nombre_us + ' ' + mini.apellido_us + '</h6>'
                                        contc += '</div>'
                                        contc += '<p class="text-muted">' + comentario.contenido_comment + '</p>'
                                        contc += '</div>'
                                        contc += '</div>'
                                        contc += '</div>'

                                        $("#com_" + post.id_publicacion).append(contc);
                                        // }
                                    },
                                    error: function (err) {
                                        console.error(err);
                                    }
                                });




                            })
                            //Fin comentario
                            //Text area
                            cont += '<div class="form-group">'
                            cont += '<textarea id="ta_' + post.id_publicacion + '" class="form-control" cols="3"></textarea>'
                            cont += '</div>'
                            cont += '<div class="form-group">'
                            cont += '<button class="form-control" onclick="a(ta_' + post.id_publicacion + ',' + post.id_publicacion + ')"  >Comentar</button>'
                            cont += '</div>'
                            //fin text area
                            cont += '</div>'
                            cont += '</div>'
                            cont += '</div>'
                            cont += '</div>'
                            cont += '</div>'
                            $("#divPosts").append(cont);
                        }
                    },
                    error: function (err) {
                        console.error(err);
                    }
                });

            });
        },
        error: function (err) {
            console.error(err);
        }
    });
}