$(document).ready(function () {

    let token = $("meta[name='_csrf']").attr("content");
    let header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });

    // alert("loaded");
    // bind form submit event
    $("#registroUsuario").on("submit", function (e) {

        e.preventDefault();

        let username = $("#username").val();
        let password = $("#password").val();
        let nombre_us = $("#nombre_us").val();
        let apellido_us = $("#apellido_us").val();
        let fc_nac_us = $("#fc_nac_us").val();
        let genero_us = parseInt($("#genero").val());
        let token = $("#token").val();

        let arrayMail = username.split("@");
        console.log(arrayMail[0].length);
        //const regex = /^[0-9]*$/;
        if (token.length > 0) {
            if (arrayMail[1] == "uteq.edu.mx") {
                $.ajax({
                    url: $("#registroUsuario").attr('action'),
                    type: "POST",
                    data: {
                        username: username,
                        password: password,
                        nombre_us: nombre_us,
                        apellido_us: apellido_us,
                        fc_nac_us: fc_nac_us,
                        genero_us: genero_us,
                        token: token
                    },
                    dataType: 'json',
                    success: function (res) {
                        console.log(res);
                        toastr['success'](
                                'Hola ' +
                                nombre_us +
                                ", bienvenido, ya puedes inicar sesión <br> <i class='fas fa-spinner fa-pulse'></i>",
                                )
                        //alert(res.message);
                        window.setTimeout(() => {
                            window.location.href = 'http://localhost:8080/login/'
                        }, 1000)
                    },
                    error: function (err) {
                        toastr['warning']("Token incorrecto, por favor revisa tu correo electronico")
                    }
                });
            } else {
                //   toastr['warning']("Por favor, ingresa con un correo institucional")
            }
        } else {
            toastr['warning']("Introduce un token valido")

        }


    });
});


function solicitaToken() {
    let username = $("#username").val();
    if (username.length > 0) {
        $.ajax({
            url: "http://localhost:8080/files/solicitaToken/",
            type: "POST",
            data: {
                username: username,
            },
            dataType: 'json',
            success: function (res) {
                console.log(res);
                toastr['success'](
                        'Token enviado al correo ' +
                        username +
                        ", por favor, ingresalo para continuar el registro ",
                        )

            },
            error: function (err) {
                console.error(err);
            }
        });

    } else {
        toastr['warning']('Ingresa un correo electronico')
    }

}


